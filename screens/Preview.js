import React, { Component } from 'react'
import { StyleSheet,View,Text,TouchableOpacity,Dimensions, Keyboard,TouchableWithoutFeedback, ImageBackground, Button, Switch, Image } from 'react-native'; 
import { LinearGradient } from 'expo-linear-gradient';
import { Camera } from 'expo-camera';
import Logo from '../components/Logo';
import * as Permissions from 'expo-permissions';
import { withNavigation } from 'react-navigation';
import * as Print from 'expo-print';
import QRCode from 'react-native-qrcode-svg';


class Preview extends Component {
    constructor(props) {
        super();
        this.state = {
            selectedPrinter: null,
            passingData: {},
            inputt: {},
            purpose: false,
            deviceWidth: Math.round(Dimensions.get('window').width),
            hasPermission: null,
            capturing: true,
            displayButtonRight: "Take Photo",
            displayButtonLeft: "Back",
            badgeImage: null,
            photoUri: "",
            cameraOn: false,
            QRCodeId: "",
            baseUrl: 'http://visitorapp.eastus.cloudapp.azure.com:8000',
        }
    }


/**********************************
 ****** ON COMPONENT MOUNT *******
***********************************/

//Require Permissions, QRCode & Get Visitor Information 
async componentDidMount() {
    const passingData = this.props.route.params;
    const qrcodeid = JSON.stringify(passingData.visitorId)

    this.setState({
        inputt: passingData,
        QRCodeId: qrcodeid
    });
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasPermission: status === 'granted' });
}
 
/**********************************
 ******** FETCH FUNCTION **********
***********************************/

//Nothing is calling this right now
addVisitor = () =>{
    const visitorInfo = this.state.inputt
    const baseUrl = this.state.baseUrl
    const photo = this.state.badgeImage
    const checkin = new Date().toISOString();;
    let method;
    let param;
    let body = JSON.stringify({
        "visitor": {
            "firstName": visitorInfo.CheckinObject.visitor.firstName,
            "lastName": visitorInfo.CheckinObject.visitor.lastName,
            "email":  visitorInfo.CheckinObject.visitor.email,
            "phoneNumber":  visitorInfo.CheckinObject.visitor.phoneNumber,
            "acceptTC": true,
            "photo": photo
        },
        "schedule_id":  1,
        "employee_id":  visitorInfo.CheckinObject.employee_id,
        "purpose_id":  visitorInfo.CheckinObject.purpose_id,
        "checkin": checkin,
        "checkout": null,
        "company": visitorInfo.CheckinObject.company,
        "hostStatus": null
    });
    if(visitorInfo.visitorId === 0){
         param = '/api/v1/visit/new'
         method = 'POST'
     }
     else{
         param = '/api/v1/visit/'
         method = 'PATCH'
     }

     const url = `${baseUrl}${param}`
     let headers = new Headers();
     headers.append('Content-Type', 'application/json; charset=utf-8');
     let req = new Request(url, {
       method: method,
       body: body,
       headers: headers,
       mode: 'cors'
   })
     fetch(req)
     .then((response) => {
       return response.json();
     })
     .then((data) => {
        console.log(data);      
     });

}


/**********************************
 ****** HANDLE BUTTONS *******
***********************************/

buttonHandler = (value) => {
    switch(value) {
        case "Take Photo":
        this.getCamera()
          break;
        case "Print":
       // this.addVisitor()
        this.printHTML();
          break;
        case "Back":
          this.handleBack()
          break;
        case "Retake":
          this.getCamera()
          break;
        default:
         alert("Error")
      }
}

handleBack = () =>{
    this.props.navigation.navigate('Guest')
}

/**********************************
 ******* CAMERA FUNCTIONS *********
***********************************/

//Determine Camera state and set photo data to state, Dynamically Change Button Option
async getCamera()
    {
    if (this.state.capturing)
        {  
        const options = { base64: true };
        let photo = await this.camera.takePictureAsync(options);
        this.setState({ capturing: false, displayButtonLeft: 'Back', displayButtonRight: 'Print', badgeImage: photo.base64, imageUri: photo.uri});
        this.camera.pausePreview()

        }
        else
        {
            this.camera.resumePreview();
            this.setState({ capturing: true, badgeImage: null});
        }
}

//Turn Camera Off or On
toggleCamera = (value) => {
        this.setState({ cameraOn: value, badgeImage: null });   
}

/**********************************
 ******* PRINT FUNCTIONS *********
***********************************/

//Pick a Printer, In production will just be assigned
selectPrinter = () => {
    const selectedPrinter = Print.selectPrinter({ x: 100, y: 100 })
    this.setState({ selectedPrinter })
  }

 //Print HTML with Dynamic Badge & navigate to final page
 async printHTML() {
    let badgename = `${this.state.inputt.CheckinObject.visitor.firstName} ${this.state.inputt.CheckinObject.visitor.lastName}`;
    var htmlString = 
                `<div style="box-shadow:0 4px 8px 0 rgba(0,0,0,0.2);transition: 0.3s;width: 40%;"> 
                    <img src="data:image/jpg;base64, ${this.state.badgeImage}" style="width:100px; height: 100px"/> 
                    <div style="padding: 2px 16px;"> 
                        <h4><b>${badgename}<b></h4>
                        <p>${this.state.inputt.CheckinObject.company}</p>
                    </div>
                </div>`
    await Print.printAsync({
     html: htmlString
    })
     // this.addVisitor()
    this.props.navigation.navigate('Finish')
}  

/**********************************
 ******* STYLESHEET *********
***********************************/


styles = StyleSheet.create({
    button: {
        borderWidth:1, 
        height:45, 
        padding:10, 
        width:200, 
        borderColor: "#187FC0", 
        borderRadius: 8, 
        display: "flex", 
        justifyContent:"center", 
        alignItems:"center"
    }
});

render(){

// CAMERA ON BUTTONS VIEW OPTION    
    const CameraOnButtons = () => (
                <View style={{flex:2, flexDirection:"row", justifyContent:"space-between", alignItems:"center", zIndex: -1, margin: 12, top:220}}>
                    <TouchableOpacity style={{...this.styles.button}}>
                        <Text style={{fontSize:20, color: "#187FC0"}}onPress={() => this.buttonHandler(this.state.displayButtonLeft)}>{this.state.displayButtonLeft}</Text>
                    </TouchableOpacity>
                    <LinearGradient style={{...this.styles.button, borderWidth:0}} colors={['#61A7D4', '#0072B9']} start={[0.6, -0.2]} end={[0.4, 1]}>
                        <Text style={{fontSize:20, color: "white"}}onPress={() => this.buttonHandler(this.state.displayButtonRight)}>{this.state.displayButtonRight}</Text>
                    </LinearGradient>
                </View> 
    );
 // CAMERA OFF BUTTONS VIEW OPTION   
    const CameraOffButtons = () => (         
                <View style={{flex:2, flexDirection:"row", justifyContent:"space-between", alignItems:"center", zIndex: -1, margin: 12, top:820}}>
                        <TouchableOpacity style={{...this.styles.button}}>
                            <Text style={{fontSize:20, color: "#187FC0"}}onPress={() => this.handleBack('Back')} >Back</Text>
                        </TouchableOpacity>
                        <LinearGradient style={{...this.styles.button, borderWidth:0}} colors={['#61A7D4', '#0072B9']} start={[0.6, -0.2]} end={[0.4, 1]}>
                            <Text style={{fontSize:20, color: "white"}}onPress={() => this.buttonHandler('Print')}>Print</Text>
                        </LinearGradient>
                </View>
    );

    const { hasPermission } = this.state
    const badgeImage = require('../assets/badgeimage.png')
    const buttons = this.state.cameraOn ? <CameraOffButtons/> : <CameraOnButtons/>;
        if (hasPermission === null) {
            return <View />; 
        } else if (hasPermission === false) {
            return <Text>Please Allow Access For the Camera</Text>;
        } else {
            return (
    <View>
            {/* LOGO VIEW */}
                <View style={{flex:1, backgroundColor:'#FBFBFB'}}>
                        <View style={{flex:1}}>
                            <Logo width={this.state.deviceWidth} height={137}/>
                        </View>
                </View>

            {/* SWITCH CAMERA OPTION VIEW */}
                <View style={{ top: "150%", left: "9%", alignItems: 'center' }} >
                    <Text style={{fontSize: 20}}>Skip taking photo</Text>
                </View>
                <View style={{ top: "130%", left: "23%", alignItems: 'center' }}>
                        <Switch
                            onValueChange={(value) => this.toggleCamera(value)}
                            value={this.state.cameraOn}/>
                </View>

            {/* BADGE IMAGE, CAMERA & NAME DISPLAY VIEW */}
            <View>
                    <View style={{width: 400, height: 50,left: "23%", top: "350%",  alignItems: "center"}}>
                        <ImageBackground style={{flex: 1, height: 655, width: 555 }} resizeMode="contain" source={badgeImage}>
                                
                            {/* CAMERA OPTION */}
                            <View style={{ display: this.state.cameraOn ? 'none' : 'flex'  }}>
                                <Camera style={{ height: 240, width: 240, borderRadius: 120,  overflow: 'hidden', top:"10%", left:"28%", marginBottom: 75}}
                                    type={Camera.Constants.Type.front} ref={ref => {this.camera = ref}} >
                                </Camera>
                            </View>
            
                            {/* FIRST LETTER OPTION */}
                            <View style={{ display: this.state.cameraOn ? 'flex' : 'none', height: 240, width: 240, borderRadius: 120,  overflow: 'hidden', top:"10%", left:"28%", marginBottom: 75, backgroundColor: "#FBFBFB", alignItems: "center"}}>
                                <Text style={{fontSize:95,top:"30%" }}>
                                        { (this.state.inputt.CheckinObject.visitor.firstName).charAt(0)}
                                </Text>  
                            </View>
                                
                            {/* VISITOR NAME */}
                            <View style={{ alignItems: "center", bottom:"35%"}}>
                                    <Text style={{fontSize: 22}}>{this.state.inputt.CheckinObject.visitor.firstName} {this.state.inputt.CheckinObject.visitor.lastName}</Text>
                                    <Text style={{fontSize: 22}}>{this.state.inputt.CheckinObject.company}</Text>              
                            </View>

                            {/* QR CODE */}
                            <View style={{left:"41%", top:"185%" }}>
                                        <QRCode value={this.state.QRCodeId}/>
                            </View>    
            
                        </ImageBackground>
                    </View>
            </View>     

            {/* BUTTONS VIEW  */}
            <View>{buttons}</View>                  
    </View>
   
     );
}}}


export default withNavigation(Preview);

