import React, { Component } from 'react'
import { Modal ,Image, StyleSheet, View, Text, Alert, TouchableOpacity,Animated, Dimensions, Keyboard, TouchableWithoutFeedback } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import FloatingInput from '../components/FloatingInput';
import SearchBar from '../components/SearchBar'
import Logo from '../components/Logo';
import TermsConditions from '../components/TermsConditions'
import Dropdown from '../components/Dropdown'

export default class GuesInfo extends Component{
  constructor () {
    super();
    this.state = {
      visitorInfor:{},
      input:{},
      purpose: false,
      deviceWidth: Math.round(Dimensions.get('window').width),
      isChecked: false,
      showTC: false,
      redRequiredInput: {
        firstName: false,
        lastName: false,
        email: false,
        host: false,
        purpose: false,
        check: false
      },
      canGo:false,
      shift: new Animated.Value(1),
      needShift: false,
      basicURL: 'http://visitorapp.eastus.cloudapp.azure.com:8000',
      displayDropdown: false
    }
  }

  getInfo = () => {
    return
    const idOrEmail = this.props.route.params
    const param = idOrEmail.id == 0? idOrEmail.email : idOrEmail.id
    const url = `${this.state.basicURL}/api/v1/schedule?email_or_visitor_id=${param}`
    fetch(url)
    .then(res => res.json())
    .then(data =>{
      let visitorInfor;
      if(data.length != 0){ // visitor info is exist
        if(data[0].employee){ // a visitor has invitation
          const {visitor: {id, firstName, lastName, email, phoneNumber }, employee } = data[0]
          visitorInfor = {
            id, firstName, lastName, email,
            phone: phoneNumber,
            host: employee.firstName + " " + employee.lastName
          }
          this.setState({hostObj: employee})

        } else{ // a visitor has no invitation
          const{id, firstName, lastName, email, phoneNumber} = data[0]
          visitorInfor = {
            id, firstName, lastName, email,
            phone: phoneNumber
          }
        }
       this.setState({input: visitorInfor, old: visitorInfor}) 
      } else { // visitor info is not exist
          visitorInfor = {
            email: idOrEmail.email
          }
          this.setState({input: visitorInfor})
      }
    })

  }


  styles = StyleSheet.create({
    container: {
      flex: 7.5,
      flexDirection: 'column',
      padding: 15,
      paddingLeft: 60,
      paddingRight: 60,
    },
    form: {
      backgroundColor:'white',
      flex:15,
      shadowColor: '#000',
      shadowOpacity: 0.07,
      shadowOffset: {width:3,height:0},
      shadowRadius: 22,
      paddingTop: 20,
      paddingBottom: 20,
      paddingLeft:38,
      paddingRight:38,
      borderRadius: 8
    },
    line: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'row'
    },
    button: {
      borderWidth:1, 
      height:45, 
      padding:10, 
      width:200, 
      borderColor: "#187FC0", 
      borderRadius: 8, 
      display: "flex", 
      justifyContent:"center", 
      alignItems:"center"
    }
  });

  hideTC = () =>{
    this.setState({showTC: false})
  }

  showTC = () =>{
    this.setState({showTC: true})
  }

  firstNameChange = (text) => {
    this.setState({input:{ ...this.state.input, firstName: text}, redRequiredInput:{...this.state.redRequiredInput, firstName: false}})

  }

  lastNameChange = (text) => {
    this.setState({input:{ ...this.state.input, lastName: text }, redRequiredInput:{...this.state.redRequiredInput, lastName: false}})
  }
  
  emailChange = (text) => {
    this.setState({input:{ ...this.state.input, email: text }, redRequiredInput:{...this.state.redRequiredInput, email: false}})
  }

  phoneChange = (text) => {
    this.setState({input:{ ...this.state.input, phone: text }})
  }

  companyChange = (text) => {
    this.setState({input:{ ...this.state.input, company: text }})
  }


  hostChange = (text, hostObj) => {
    this.setState({input:{ ...this.state.input, host: text }, redRequiredInput:{...this.state.redRequiredInput, host: false}, hostObj})
  }

  purposeChange = (text) => {
    this.setState({input:{ ...this.state.input, purpose: text }, redRequiredInput:{...this.state.redRequiredInput, purpose: false}})
  }

  hideDropdown = ()=>{
    this.setState({displayDropdown: false})
  }

  showDropdown = () =>{
      this.setState({displayDropdown: true})
      Keyboard.dismiss()
  }

  isEmailRightFormat = () => {
      if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.state.input.email))
      {
        return (true)
      }
        return (false)
  }

  checkIfFilled = () => {
    let newInfo = this.state.input
    let isEmailRightFormat = this.isEmailRightFormat()

    const requiredFeild = ['firstName', 'lastName', 'email', 'host', 'purpose']

    let red = {};

    requiredFeild.forEach(feild => { //set false by default
      red = {...red, [feild]: false}
    })

    requiredFeild.forEach(feild => { // if not filled, set red to true
      if(!newInfo[feild]){
        red[feild] = true
      }
    })

    let filled = requiredFeild.filter(feild => !red[feild])
    
    if(JSON.stringify(filled) == JSON.stringify(requiredFeild)){

      let redRequiredInput = {}

      requiredFeild.forEach(feild => { //set false by default
        redRequiredInput = {...redRequiredInput, [feild]: false}
      })

      if(!isEmailRightFormat){ //check the email is right format
        redRequiredInput.email = true
        this.setState({redRequiredInput})
        alert("You have entered an invalid email address!")
        return
      } else if(!this.state.hostObj){ // check the host is exist
        redRequiredInput.host = true
        this.setState({redRequiredInput})
        alert("Your host is not exist!")
        return
      } else if (!this.state.isChecked){ // check if the T&C has been accepted
        redRequiredInput.check = true
        this.setState({redRequiredInput})
        alert("Pleas accept the Terms & Conditions!")
        return
      }

      this.setState({redRequiredInput})

      if(newInfo.id){ // if the visitor is exist
        this.goNext(newInfo.id)
      } else {// if visitor not exist
        this.goNext(0)
      }

    } else {
      this.setState({redRequiredInput: red})
      alert('Please fill the required field!')
    } 
  }

  goNext = (id) => {
   
    const passingData = {
      visitorId: id,
      CheckinObject: {
        "visitor": {
          "firstName": this.state.input.firstName,
          "lastName": this.state.input.lastName,
          "email": this.state.input.email,
          "phoneNumber": this.state.input.phoneNumber,
          "photo": null,
          "signature": "",
          "acceptTC": true
      },
      "employee_id": this.state.hostObj.id,
      "purpose_id": 1,
      "checkin": null,
      "company": this.state.input.company,
      }
      
    }

    this.props.navigation.navigate('Preview', passingData)

  }

  needShift = () =>{
    this.setState({needShift: true})
  }

  noNeedShift = () =>{
    this.setState({needShift: false})
  }

  backHome = () => {
    Alert.alert(
      'Cancel Check-in?',
      'All the filled information will be lost.',
      [
        {
          text: 'Yes, cancel',
          onPress: () => this.props.navigation.navigate('QrScanner', 0),//back to reset the scan times
          style: 'cancel',
        },
        {text: 'No', onPress: () => console.log('Nothing')},
      ],
      {cancelable: false},
    );
  }

  componentDidMount(){
     this.getInfo();
  };

  componentDidUpdate(){
    Animated.timing(this.state.shift, {
      toValue: this.state.needShift ? 0 : 1,
      duration: 200,
    }).start()
  }

  render(){

    const style = {
      flex:1, 
      backgroundColor:'#FBFBFB', 
      height: 115, 
      position: 'reletive',
      bottom: this.state.shift.interpolate({
        inputRange: [0, 1],
        outputRange: [Dimensions.get('window').height * 0.16, 0]
      })
    }

    return (
      <TouchableWithoutFeedback onPress={()=>{
        Keyboard.dismiss()
        this.setState({displayDropdown: false})
        }}>
      <Animated.View style={style}>
        <View style={{flex:1}}>
            <Logo width={this.state.deviceWidth} height={137}/>
        </View> 
        <View style={{...this.styles.container}}>

        <View style={this.styles.form}>

          <View style={{...this.styles.line, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start'}}>
            <LinearGradient style={{borderWidth:0, width:10, height:38}} colors={['#61A7D4', '#0072B9']} start={[0.6, -0.2]} end={[0.4, 1]}></LinearGradient>
            <Text style={{fontSize: 25, fontWeight:'700', marginLeft: 10}}>Visitor</Text>
          </View>

            <View style={{...this.styles.line}}>

              <View style={{flex:1, flexDirection:"row"}}>
                <FloatingInput label="First Name*" floatColor = "#187FC0" marginRight ={5} red={this.state.redRequiredInput.firstName}
                onChangeText={this.firstNameChange} value={this.state.input.firstName} hideDropdown={this.hideDropdown} />
              </View>

              <View style={{flex:1, flexDirection:"row"}}>
              <FloatingInput label="Last Name*" floatColor = "#187FC0" marginLeft ={5} red={this.state.redRequiredInput.lastName} 
              onChangeText={this.lastNameChange} value={this.state.input.lastName} hideDropdown={this.hideDropdown}/>
              </View>

            </View>

            <View style={{...this.styles.line}}>
              <FloatingInput label="E-mail*" floatColor = "#187FC0" red={this.state.redRequiredInput.email} keyboardType="email-address"
              onChangeText={this.emailChange} value={this.state.input.email} hideDropdown={this.hideDropdown}/>
            </View>

            <View style={{...this.styles.line}}>
              <View style={{flex:1, flexDirection:"row"}}>
                <FloatingInput label="Phone Number" floatColor = "#187FC0" marginRight ={5} onChangeText={this.phoneChange} 
                value={this.state.input.phone} hideDropdown={this.hideDropdown} keyboardType="numeric"/>
              </View>
              <View style={{flex:1, flexDirection:"row"}}>
                <FloatingInput label="Company" floatColor = "#187FC0" marginLeft ={5}  onChangeText={this.companyChange} value={this.state.input.company} hideDropdown={this.hideDropdown}/>
              </View>
            </View>

            <View style={{...this.styles.line, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start', marginTop:30}}>
              <LinearGradient style={{borderWidth:0, width:10, height:38}} colors={['#61A7D4', '#0072B9']} start={[0.6, -0.2]} end={[0.4, 1]}></LinearGradient>
              <Text style={{fontSize: 25, fontWeight:'700', marginLeft: 10}}>Visits</Text>
            </View>

            <View style={{...this.styles.line, zIndex: 1}}>
              <SearchBar needShift={this.needShift} noNeedShift={this.noNeedShift} value={this.state.input.host} width={this.state.deviceWidth - 196} hostChange={this.hostChange} red={this.state.redRequiredInput.host} hideDropdown={this.hideDropdown}/>
            </View> 

            <View style={{...this.styles.line}}>
              <Dropdown width={this.state.deviceWidth - 196} value={this.state.input.purpose} display={this.state.displayDropdown} hideDropdown={this.hideDropdown} red={this.state.redRequiredInput.purpose} showDropdown={this.showDropdown} purposeChange={this.purposeChange}/>
            </View> 

            <View style={{...this.styles.line, justifyContent:'flex-start', zIndex:-1}}>
              <TouchableWithoutFeedback onPress={()=>{
                if(this.state.isChecked){
                  this.setState({isChecked: false})
                } else {
                  this.setState({isChecked: true,redRequiredInput:{...this.state.redRequiredInput, check: false}})
                }
              }}>
                <View style={{flexDirection: 'row', paddingVertical:20}}>
                  <View style={{ 
                          width:23, 
                          height:23, 
                          backgroundColor: this.state.isChecked? '#66BB7F' : '#fff',
                          borderWidth: this.state.redRequiredInput.check? 2 : 1,
                          borderColor: this.state.redRequiredInput.check? '#AD1717' : '#66BB7F' ,
                          borderRadius: 4, 
                          justifyContent:'center',
                          alignItems:'center'
                        }
                  }>
                    <Image style={{
                      width:22,
                      height:22,
                      resizeMode: 'contain',
                    }} source={require('../assets/check.png')} />
                  </View>
                <Text style={{fontSize: 20, marginLeft:10}}>I accept the </Text>
              </View>
              </TouchableWithoutFeedback>
              <TouchableOpacity onPress={this.showTC}>
                <Text style={{fontSize: 20, color: '#66BB7F', textDecorationLine:'underline'}} >Terms &amp; Conditions</Text>
              </TouchableOpacity>
            </View>

          </View> 

          <View style={{flex:2, flexDirection:"row", justifyContent:"space-between", alignItems:"flext-startz", zIndex: -1, paddingTop: 40}}>
            <TouchableOpacity style={{...this.styles.button}} onPress={this.backHome}>
              <Text style={{fontSize:20, color: "#187FC0"}}>Cancel</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{...this.styles.button}} onPress={this.checkIfFilled}>
              <LinearGradient style={{...this.styles.button, borderWidth:0}} colors={['#61A7D4', '#0072B9']} start={[0.6, -0.2]} end={[0.4, 1]}>
                <Text style={{fontSize:20, color: "white"}}>Next</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>

        </View>

        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.showTC}
        >
          <TermsConditions hideTC={this.hideTC}/>
        </Modal>

      </Animated.View>
      </TouchableWithoutFeedback>
    );
  }
}
