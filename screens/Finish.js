import React, { Component } from 'react'
import { View, TouchableWithoutFeedback,Dimensions, Text, Image } from 'react-native'
import Logo from '../components/Logo';
import Gif from '../assets/Gif.png'

export default class Finish extends Component{

    state = {
        name: "",
        deviceWidth: Math.round(Dimensions.get('window').width),
    }

    backHome = () =>{
      console.log("Back home now!!")
      this.props.navigation.navigate('QrScanner')
    }

    componentDidMount(){
       // Get the name passed from last screen, 
       //↓↓↓↓↓↓↓↓↓↓↓↓ When add the screen, add this lines ↓↓↓↓↓↓↓↓↓↓↓↓

       // let firstName = this.props.route.params
       // this.setState({name: firstName})

        this.setState({name: 'Amber'}) // This is just for test
        setTimeout(this.backHome, 5000)
    }

    render(){

        return(
            <TouchableWithoutFeedback onPress={this.backHome}>
                <View style={{flex:1, backgroundColor:'#FBFBFB'}}>
                    <View style={{flex:1}}>
                        <Logo width={this.state.deviceWidth} height={137}/>
                    </View> 
                    <View style={{flex: 7.5,justifyContent:'center', alignItems:'center', paddingTop:120}}>
                      <View style={{ justifyContent:'center', alignItems:'center', height:420, backgroundColor:'white', width:420, borderRadius:300,shadowColor: '#000', shadowOpacity: 0.07, shadowOffset: {width:3,height:0}, shadowRadius: 22,}}>
                        <Image source={Gif} style={{width:224}}/>
                      </View>
                      <View style={{flex:3,justifyContent:'flex-start',alignItems:'center'}}>
                        <Text style={{fontSize:40,fontWeight:'700', marginTop:50}}>Welcome, {this.state.name}!</Text>
                        <Text style={{fontSize:40,fontWeight:'700'}}>Your host has been notified.</Text>
                        <Text style={{fontSize:24,color:'#484848' ,fontWeight:'100', marginTop:10}}>Please take your badge and don’t forget to check-out!</Text>
                      </View>
                      <Text style={{marginBottom:80, color:"#47AD65", textDecorationLine: 'underline'}}>Touch screen to return home</Text>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        )
    }
}