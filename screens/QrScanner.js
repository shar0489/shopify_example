import React, { useState, useEffect } from 'react';
import { Modal, Text, View, StyleSheet, Animated, Dimensions, TouchableOpacity, Image  } from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import { LinearGradient } from 'expo-linear-gradient';

import FloatingInput from '../components/FloatingInput';
// import Logo from '../components/Logo';
import CheckOut from '../components/ChekOut'
import WelcomeImage from '../assets/welcome.png'

export default function QrScanner({navigation}) {

  ////states////////
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);

  const [visitorEmail, setVisitorEmail] = useState("");
  const [visitorID, setVisitorID] = useState(0);
  const [modalVisible, setModalVisible] = useState(false);
  const [shift, setShift] = useState(new Animated.Value(1));
  const [needShift, setNeedShift] = useState(false);


    ////baseURL to be changed when deployed to cloud
  //const baseUrl = "http://localhost:8000"
 
  const baseUrl = "http://visitorapp.eastus.cloudapp.azure.com:8000"     ////baseURL to be changed when deployed to cloud

  // http://visitorapp.eastus.cloudapp.azure.com:8000/api/v1/visitByVisitor/1

  let newId = 0
  let scannedTimes = 0

  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, []);


///test data

  const myJson = {
    id: 3,
    visitor: {
        id: 1,
        firstName: "Eric",
        lastName: "Cartman",
        email: "clauber.ferreira@gmail.com",
        phoneNumber: "+16478750656",
        photoUrl: null,
        signature: "",
        acceptTC: true
    },
    schedule: null,
    employee: {
        id: 1,
        firstName: "Clauber",
        lastName: "Ferreira",
        email: "clauber.ferreira@gmail.com",
        phoneNumber: "+16478750656"
    },
    checkin: "2020-02-13T02:00:00Z",
    checkout: null,
    company: "Algonquin",
    hostStatus: null
}



/////get the scanner data
  const handleBarCodeScanned = ({ type, data }) => {

    //console.log("scanned" + data);
    setVisitorID(data);
    //console.log("visitorID", visitorID);

    isVisitorChecked(data);
  
    // setScanned(true);
    // alert(`Bar code with type ${type} and data ${data} has been scanned!`); 

  };


  ////checkout 

  const checkoutFunction = (visitId)=> {

    setModalVisible(true); 

    //add the patch to checkout time 
    const t = Date.now()

    let checkoutTime = JSON.stringify(new Date(t));

    let body = { "checkout": checkoutTime}

    const url = `${baseUrl}/api/v1/visit/${visitId}/`;

    let h = new Headers();
    h.append('Content-Type', 'application/json; charset=utf-8');
    //h.append('Authorization', 'Bearer + {{token}}')
    let req = new Request(url, {
      method: 'PATCH',

      body: body,

      headers: h,
      mode: 'cors'
  })
    fetch(req)
    .then((response) => {
      return response.json();
    })
    .then((myJson) => {

      //console.log(myJson);      
    });
  }
  
  //navigation and passing info
  const navigateToGuestInfo = (visitorInfo) => {
    navigation.navigate('Guest', visitorInfo)   ///////i will pass visitorID or email
    // setVisitorID(0);
    // setVisitorEmail("");
  }

  ///checkin
  const checkinFunction = () => {
    //console.log("checkin");
    let visitorInfo = {
      id: newId,
      email: visitorEmail
    }
    //fetch the user to checkIn

    //console.log(visitorInfo)
    //pass information to the next screen
    //navigate to the next screen

    // let visitorInfo =  {
    //   id: 12345,
    //   firstName: 'Mack',
    //   lastName: 'World',
    //   email: '',
    //   phone: '3439273849',
    //   company: 'Algonquin College',
    //   host: 'Amber Yiyao',
    //   purpose: 'Attend Meeting',
    //   isChecked: false
    // };
    navigateToGuestInfo(visitorInfo)


  }

////chcecks if visitor already checkedin 


const isVisitorChecked = (id) => {
  //console.log('times', scannedTimes)
  if(!id){
    return
  }

  if(scannedTimes != 0){
    return
  }

  scannedTimes++
  
  //console.log("chckoutdata", id);
  // const url1 = `${baseUrl}/api/v1/visitByVisitor/${data}/`;
  setVisitorID(id)

  newId = id

  const url1 = `${baseUrl}/api/v1/visit/?email_or_visitor_id=${id}/`;
  console.log(url1)

  let h = new Headers();

  h.append('Content-Type', 'application/json; charset=utf-8');
  //h.append('Authorization', 'Bearer + {{token}}')
  let req = new Request(url1, {
      method: 'GET',
      headers: h,
      mode: 'cors'
        })

  fetch(req)
  .then((response) => {
    // console.log(response.clone().body);
    return response.text();
  })
  .then((myJson) => {
    // console.log("mack" + myJson);
    let amber = JSON.parse(myJson)
    //console.log(amber );
    // console.log("checkin: " + amber.checkin);
    // console.log("checkout: " + amber.checkout)
    // myJson.checkin = true;
    // myJson.checkout = null;
    // myJson.id = 2; 
    if (amber.checkin) {
      checkoutFunction(amber.id)
    } else {
      checkinFunction();
    }
  }).catch(err => console.log(err));

//   let test1 = true;
//   let test2 = null;
// console.log(test1 + "   " + test2)
  
//     if (test1 && test2 === null ) {
//       // let outTime = Date.now();
//       checkoutFunction(2)

//     } else {
//       checkinFunction();
//     }


}

/////handle camera permission
  if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }


  //////styles
  const styles = StyleSheet.create({
    container: {
      flex: 5,
      // backgroundColor: '#fff',
      flexDirection: 'column',
      padding: 15,
      paddingLeft:70,
      paddingRight:70
    },
    line:{
      flex: 1,
      // backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'row',
      marginBottom: 100
    },
    button:{
      borderWidth:0, height:45, padding:10, width:200, borderColor: "#187FC0", borderRadius: 8, display: "flex", justifyContent:"center", alignItems:"center"
    }
  });

///////email input box handler

  const emailChange = (text) => {
    setVisitorEmail(text);
    // isVisitorChecked(text);

    // console.log(this.state.input)

  }

  ////modal checkout expiry
  const closeModal = () => {
    setTimeout(() => {setModalVisible(false)}, 4000) 
  }

  const windoeNeedShift = () =>{
    setNeedShift(true)
  }

  const noNeedShift = () =>{
    setNeedShift(false)
  }
  

  // Animated.timing(shift, {
  //   toValue: needShift ? 0 : 1,
  //   duration: 200,
  // }).start()

////////////////////screen

const deviceWidth=Dimensions.get('window').width
const ratio = deviceWidth/1620

  return (
    <View
      style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end',
        // position: 'reletive',
        // bottom: shift.interpolate({
        //   inputRange: [0, 1],
        //   outputRange: [Dimensions.get('window').height * 0.14, 0]
        // })
      }}>

        <View style={{flex:4}}>
            {/* <Logo width={Math.round(Dimensions.get('window').width)} height={137}/> */}
            <Image source={WelcomeImage} style={{width: deviceWidth.width, height: ratio*860}} resizeMode="contain"/>
        </View>

        <View style={{backgroundColor:'white',flex:7, marginHorizontal:57, paddingHorizontal: 40 ,borderRadius: 8, shadowColor: '#000',
          shadowOpacity: 0.07,
          shadowOffset: {width:3,height:0},
          shadowRadius: 22,}}>

          <View style={{flex:1,flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start',marginTop:20}}>
              <LinearGradient style={{borderWidth:0, width:8, height:32}} colors={['#61A7D4', '#0072B9']} start={[0.6, -0.2]} end={[0.4, 1]}></LinearGradient>
              <Text style={{fontSize: 20, marginLeft: 10}}>Scan your QR code:</Text>
          </View>

          <View style={{flex:4, justifyContent:'center', alignItems:'center', marginTop: 26}}>
            <BarCodeScanner
              onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
              // style={StyleSheet.absoluteFillObject}
              style={{
                width:200,
                flex: 1,
                backgroundColor:'red',
                marginVertical:20
              }}
              type={BarCodeScanner.Constants.Type.front}
            />
          </View>

          <View style={{flex:1,flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start', marginTop:20}}>
              <LinearGradient style={{borderWidth:0, width:8, height:32}} colors={['#61A7D4', '#0072B9']} start={[0.6, -0.2]} end={[0.4, 1]}></LinearGradient>
              <Text style={{fontSize: 20, marginLeft: 10}}>Enter your E-mail:</Text>
          </View>
          
          <View style={{flexDirection:"row", flex:1, marginBottom:40 }}>
            <FloatingInput label="Email*" floatColor = "#187FC0" marginRight ={5} onChangeText={emailChange} value={visitorEmail}/>
          </View>

      </View>
      

      <View style={{flex:2, flexDirection:"row", justifyContent:"flex-end", alignItems:"flex-start", marginHorizontal:57 }}>
        <TouchableOpacity style={{...styles.button, borderWidth:0, marginTop:25}} onPress={isVisitorChecked(visitorEmail)}>
            <LinearGradient style={styles.button} colors={['#61A7D4', '#0072B9']} start={[0.6, -0.2]} end={[0.4, 1]}>
              <Text style={{fontSize:20, color: "white"}} >Next </Text>
            </LinearGradient>
          </TouchableOpacity>
      </View>



      <Modal
      animationType="fade"
      transparent={true}
      visible={modalVisible}
      onShow = {closeModal}
      onPress = {closeModal}
      >
        <CheckOut/>
      </Modal>


      {/* {scanned && <Button title={'Tap to Scan Again'} onPress={() => setScanned(false)} />} */}
    </View>
  );
}

