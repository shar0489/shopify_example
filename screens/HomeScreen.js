import * as React from 'react';
import { View, TouchableHighlight, StatusBar, AsyncStorage } from 'react-native';
import {Video} from 'expo-av'


export default function HomeScreen({ navigation }) {


  const baseUrl = "http://visitorapp.eastus.cloudapp.azure.com:8000";

  console.log(baseUrl);

  let employeesList = [];
// store employees list to local storage



  const storeData = async () => {
    let time1= Date.now();
    let data = [{timeStamp: time1}, employeesList]
    let data1 = JSON.stringify(data)
    try {
      await AsyncStorage.setItem('storedEmployeeList', data1);
    } catch (error) {
      // Error saving data
      console.log("Error retriving from local storage")
    }
  };

  //check is employees data has been checked in last 24hours
  const retrieveData = async () => {
    try {
      console.log("amber")
      const value = await AsyncStorage.getItem('storedEmployeeList');
      console.log(value)
      
      if (value !== null) {
        employeeList = JSON.parse(storeEmployeeList)
        let timeNow = Date.now();
        if (timeNow - employeeList[0].timeStamp < 86400) {
          console.log( "No need to Fetch")

        } else {
          console.log("fetch")

          fetchEmployees();  
          


        }
      } else {
        console.log("fetch")
        fetchEmployees();
        
      }
    } catch (error) {
      // Error retrieving data
    }
  };


// fetch employees every 24 hours and store them to localstorage

const fetchEmployees = () => {
  console.log("start fetch")
    const url = `${baseUrl}/api/v1/employee/`;
    console.log(url)
  //   let h = new Headers();
  //   h.append('Content-Type', 'application/json; charset=utf-8');
  //   // h.append('Authorization', 'Bearer + {{token}}')
  //   let req = new Request(url, {
  //     method: 'GET',
  //     headers: h
  // })
    fetch(url)
    .then((response) => {
      return response.json();
    })
    .then((employees) => {
      console.log(employees);
      employeesList = employees;
      storeData(); 
    }).catch(err => console.log(err));

}

retrieveData();

    return (

   

      <View>

<StatusBar 
          hidden={true}
          barStyle={'light-content'}
          networkActivityIndicatorVisible={true}
      />
          <TouchableHighlight onPress={() => navigation.navigate('QrScanner')}>
              <Video 
                    // source={{ uri: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4' }}
                    source={require('../assets/syntronic-480.mov')}
                    rate={1.0}
                    volume={0}
                    isMuted={true}
                    resizeMode="cover"
                    shouldPlay
                    isLooping
                    style={{ width: 780, height: 1100 }}
                  />
          </TouchableHighlight>
      </View>
    );
  }