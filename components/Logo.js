import React from "react"
import Svg, {
  Defs,
  Path,
  LinearGradient,
  Stop,
  G,
  Use,
  Text,
  TSpan,
  Ellipse
} from "react-native-svg"
/* SVGR has dropped some elements not supported by react-native-svg: title, filter */

function Logo(props) {
  return (
    <Svg width={810} height={115} viewBox="0 0 810 137" {...props}>
      <Defs>
        <Path id="prefix__b" d="M590 .5h220v114H590z" />
        <Path id="prefix__d" d="M0 .5h201l26.72 57-26.72 57H0l26.72-57z" />
        <LinearGradient x1="10.008%" y1="50%" x2="100%" y2="50%" id="prefix__f">
          <Stop stopColor="#61A7D4" offset="0%" />
          <Stop stopColor="#0072B9" offset="100%" />
        </LinearGradient>
      </Defs>
      <G fill="none" fillRule="evenodd">
        <Path fill="#FBFBFB" d="M0-10h810v1091H0z" />
        <Use fill="#000" filter="url(#prefix__a)" xlinkHref="#prefix__b" />
        <Use fill="#F2F2F2" xlinkHref="#prefix__b" />
        <G opacity={0.69}>
          <Text
            fontFamily="Helvetica-Bold, Helvetica"
            fontSize={22}
            fontWeight="bold"
            transform="translate(652 34)"
          >
            <TSpan x={56} y={22} fill="#000">
              {"Step 4"}
            </TSpan>
            <TSpan
              x={56}
              y={48}
              fontFamily="Helvetica-Light, Helvetica"
              fontSize={16}
              fontWeight={300}
              fill="#707070"
            >
              {"Complete"}
            </TSpan>
          </Text>
          <G
            stroke="#717070"
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={3}
          >
            <Path d="M693 55.854v1.656a18 18 0 11-31.683-11.704 18 18 0 0121.009-4.747" />
            <Path d="M693 43.5l-17.692 18L670 56.105" />
          </G>
        </G>
        <G transform="translate(393)">
          <Use fill="#000" filter="url(#prefix__c)" xlinkHref="#prefix__d" />
          <Use fill="#F2F2F2" xlinkHref="#prefix__d" />
        </G>
        <G opacity={0.692}>
          <Text
            fontFamily="Helvetica-Bold, Helvetica"
            fontSize={22}
            fontWeight="bold"
            transform="translate(460 35)"
          >
            <TSpan x={54} y={21} fill="#000">
              {"Step 3"}
            </TSpan>
            <TSpan
              x={54}
              y={47}
              fontFamily="Helvetica-Light, Helvetica"
              fontSize={16}
              fontWeight={300}
              fill="#707070"
            >
              {"Preview"}
            </TSpan>
          </Text>
          <G
            transform="translate(460 40.5)"
            stroke="#717070"
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={3}
          >
            <Path d="M40 29.333C40 31.358 38.372 33 36.364 33H3.636C1.628 33 0 31.358 0 29.333V9.167C0 7.142 1.628 5.5 3.636 5.5h7.273L14.545 0h10.91l3.636 5.5h7.273C38.372 5.5 40 7.142 40 9.167v20.166z" />
            <Ellipse cx={20.5} cy={19} rx={7.5} ry={7} />
          </G>
        </G>
        <G filter="url(#prefix__e)" transform="translate(192)">
          <Path
            fill="url(#prefix__f)"
            d="M0 .5h201l26.72 57-26.72 57H0l26.72-57z"
          />
          <Text
            fontFamily="Helvetica-Bold, Helvetica"
            fontSize={22}
            fontWeight="bold"
            fill="#FFF"
            transform="translate(65 35)"
          >
            <TSpan x={44} y={21}>
              {"Step 2"}
            </TSpan>
            <TSpan
              x={44}
              y={47}
              fontFamily="Helvetica-Light, Helvetica"
              fontSize={16}
              fontWeight={300}
            >
              {"Information"}
            </TSpan>
          </Text>
          <G
            stroke="#FFF"
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={3}
          >
            <Path d="M81.875 41h-13.5C66.511 41 65 42.522 65 44.4v27.2c0 1.878 1.511 3.4 3.375 3.4h20.25C90.489 75 92 73.478 92 71.6V51.2L81.875 41z" />
            <Path d="M82 41v10h10M87 60.5H72M87 65.5H72M75 52.5h-3" />
          </G>
        </G>
        <G filter="url(#prefix__g)">
          <Path d="M0 .5h192.28l26.72 57-26.72 57H0z" fill="#FFF" />
          <Text
            fontFamily="Helvetica-Bold, Helvetica"
            fontSize={22}
            fontWeight="bold"
            transform="translate(36 34)"
          >
            <TSpan x={56} y={22} fill="#47AD65">
              {"Step 1"}
            </TSpan>
            <TSpan
              x={56}
              y={48}
              fontFamily="Helvetica-Light, Helvetica"
              fontSize={16}
              fontWeight={300}
              fill="#00B15C"
            >
              {"Welcome"}
            </TSpan>
          </Text>
          <Path
            d="M60 41l5.562 11.19L78 53.996 69 62.7 71.124 75 60 69.19 48.876 75 51 62.701l-9-8.705 12.438-1.806z"
            stroke="#47AD65"
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={3}
          />
        </G>
      </G>
    </Svg>
  )
}

export default Logo
