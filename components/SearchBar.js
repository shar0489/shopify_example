import React, { Component } from 'react'
import { View, Text, TouchableOpacity , AsyncStorage, } from 'react-native';
import FloatingInput from './FloatingInput';

export default class SearchBar extends Component{

    state = {
        display: false,
        hostName: this.props.value,
        hosts: [],
        searchReasult:[],
        isChanged: false,
        adminHost:{
            firstName: "Admin",
            lastName: "(Select this if need help.)",
            email:"admin@syntronic.com",
            phoneNumber: "+1392847473883",
        }
    }

    onChange = (text) => {
        let searchString = text.toLowerCase()
        
        let newHostList = this.state.hosts.filter( host => {
            let hostName = `${host.firstName} ${host.lastName}`
            if(hostName.toLocaleLowerCase().search(searchString)!= -1){
                return true
            }
        }) 

        newHostList = newHostList.map(host=>{
            return `${host.firstName} ${host.lastName}`
        }) //Just save the name of the host

        newHostList.sort( (a,b) =>{
            let aIndex = a.toLocaleLowerCase().search(searchString)
            let bIndex = b.toLocaleLowerCase().search(searchString)

            return aIndex - bIndex
        })

        newHostList.unshift(`${this.state.adminHost.firstName} ${this.state.adminHost.lastName}`)
        
        if(newHostList.length == 0 || text.length == 0){
            this.setState({hostName: text, display: false, searchReasult: newHostList, isChanged:true})

        } else {
            this.setState({hostName: text, display: true, searchReasult: newHostList, isChanged:true})
        }

        this.props.hostChange(text)
    }

    onTap = (text) =>{
        this.setState({display: false, hostName: text, isChanged:true})
        let hostObj = this.state.hosts.find(host => {
            let fullName = host.firstName + " " + host.lastName
            if( fullName == text){
              return true
            }
          })
        this.props.hostChange(text, hostObj)
    }

    getHostList = async () =>{
        if(AsyncStorage.getItem('storedEmployeeList')){
            const hosts = await AsyncStorage.getItem('storedEmployeeList')
            this.setState({hosts: JSON.parse(hosts)[1]})
        }
    }

    componentDidMount(){
        this.getHostList()
    }

    render(){

        const styles = {
            viewStyle:{
                height: 58,
                zIndex:1,
                width: this.props.width
            },
            highlightStyle:{
                paddingTop: 20, 
                paddingBottom: 20,
                paddingLeft: 20
            },
            textStyle:{
                fontSize: 20
            },
            listStyle:{
                position: "absolute",
                top: 55,
                width: this.props.width,
                borderWidth: 1,
                borderColor: 'gray',
                borderRadius: 8,
                backgroundColor: 'white',
                display: this.state.display? "flex" : "none",
                overflow: 'scroll'
            }
        }

        let n = 0
        const HostList = this.state.searchReasult.map(host => {
            n++
            if(n > 4){
                return
            }

            let fontColor = 'black'
            
            if(n == 1){
                fontColor = 'grey'
            }

            return (
                <TouchableOpacity key={n} onPress={() => this.onTap(host)} style={styles.highlightStyle}>
                    <Text style={{...styles.textStyle, color: fontColor}}>{host}</Text>
                </TouchableOpacity >
            )
        })

        return(
            <View style={styles.viewStyle}>
                <FloatingInput hideDropdown={this.props.hideDropdown} label="Host Name*"  needShift={this.props.needShift} noNeedShift={this.props.noNeedShift} red={this.props.red} onChangeText={this.onChange} floatColor = "#187FC0" value={this.state.isChanged? this.state.hostName : this.props.value} />
                <View style={styles.listStyle}>
                    {HostList}
                </View>

            </View>
        )
    }
}
