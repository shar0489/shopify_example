import React from 'react'
import { View, Text, TouchableOpacity, ScrollView, TouchableWithoutFeedback } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

export default function TermsConditions(props){

    const style = {
        overlay:{
            position: 'absolute',
            top:0,
            left:0,
            right:0,
            bottom:0,
            backgroundColor:'rgba(0, 0, 0, 0.6)',
            padding: 60,
            justifyContent:'center',
            alignItems:'center'
        },
        card:{
            backgroundColor: 'white',
            height: 900,
            borderRadius: 8, 
            padding: 50,
            paddingLeft:60,
            paddingRight: 60,
        },
        title:{
            fontSize: 20,
            fontWeight: '700',
            marginTop:20,
            marginBottom: 10
        },
        content:{
            fontSize: 20,
            lineHeight: 30

        },
        button: {
            borderWidth:1, 
            height:45, 
            padding:10, 
            width:200, 
            borderColor: "#187FC0", 
            borderRadius: 8, 
            display: "flex", 
            justifyContent:"center", 
            alignItems:"center"
          }
    }

    return(
        <View style={style.overlay}>
            <View style={style.card}>
                <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start', marginBottom: 0}}>
                    <LinearGradient style={{borderWidth:0, width:13, height:51}} colors={['#61A7D4', '#0072B9']} start={[0.6, -0.2]} end={[0.4, 1]}></LinearGradient>
                    <Text style={{fontSize: 36, fontWeight:'700', marginLeft: 10}}>Terms &amp; Conditions</Text>
                </View>
                <View style={{flex:1}}>
                <ScrollView style={{ marginTop:30, marginBottom: 40, flex:1}} >
                    <TouchableWithoutFeedback>
                        <View>
                            <Text style={style.title}>What is Lorem Ipsum?</Text>
                            <Text style={style.content}>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                            It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                            </Text>
                            <Text style={style.title}>Why do we use it?</Text>
                            <Text style={style.content}>
                            It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                            </Text>
                            <Text style={style.title}>Why do we use it?</Text>
                            <Text style={style.content}>
                            It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                            </Text>
                            <Text style={style.title}>Why do we use it?</Text>
                            <Text style={style.content}>
                            It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                            </Text>
                        </View>
                    </TouchableWithoutFeedback>
                </ScrollView>
                </View>
                <TouchableOpacity style={{alignSelf: 'flex-end'}} onPress={props.hideTC}>
                    <LinearGradient style={{...style.button, borderWidth:0}} colors={['#61A7D4', '#0072B9']} start={[0.6, -0.2]} end={[0.4, 1]}>
                        <Text style={{fontSize:20, color: "white"}}>Close</Text>
                    </LinearGradient>
                </TouchableOpacity>
            </View>
        </View>
    )
} 