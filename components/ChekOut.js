import React from 'react'
import { View, Text, Image, ScrollView, TouchableWithoutFeedback } from 'react-native';

export default function ChekOut(props){

    const style = {
        overlay:{
            position: 'absolute',
            top:0,
            left:0,
            right:0,
            bottom:0,
            backgroundColor:'rgba(0, 0, 0, 0.6)',
            justifyContent:'center',
            alignItems:'center'
        },
        card:{
            backgroundColor: 'white',
            height: 140,
            width: 460,
            borderRadius: 8,
            borderBottomWidth: 10,
            borderBottomColor: '#47AD65',
            flexDirection: 'row',
            alignItems:'center',
        },
        circle:{
            height:70,
            width:70,
            backgroundColor: '#47AD65',
            borderRadius: 50,
            alignItems:'center',
            justifyContent:'center',
            marginRight: 30,
            marginLeft: 40,
        }
    }

    return(
        <TouchableWithoutFeedback>
            <View style={style.overlay}>
                <View style={style.card}>
                    <View style={style.circle}>
                        <Image style={{
                        width:45,
                        height:45,
                        resizeMode: 'contain',
                    }} source={require('../assets/check.png')} />
                    </View>
                    <Text style={{fontSize:27, fontWeight:'700'}}>Check-out Sucessful</Text>
                </View>
            </View>
        </TouchableWithoutFeedback>
    )
}