import React, { Component } from 'react'
import { View, Text, TouchableOpacity  } from 'react-native';
import FloatingInput from './FloatingInput';


export default class Dropdown extends Component{

    render(){

        const styles = {
            viewStyle:{
                height: 58,
                width: this.props.width
            },
            highlightStyle:{
                paddingTop: 20, 
                paddingBottom: 20,
                paddingLeft: 20
            },
            textStyle:{
                fontSize: 20
            },
            listStyle:{
                position: "absolute",
                width: this.props.width,
                borderWidth:1,
                borderColor: 'gray',
                borderRadius: 8,
                backgroundColor: 'white',
                display: this.props.display? "flex" : "none"
            },
            arrow:{
                marginLeft:5,
                marginTop:1,
                width:0,
                height:0,
                borderStyle:'solid',
                borderWidth:6,
                borderTopColor:'gray',
                borderLeftColor:'#fff',
                borderBottomColor:'#fff',
                borderRightColor:'#fff',
                position:'absolute',
                right: 15,
                bottom: 16
                
            }
        }

        return(
            <View style={styles.viewStyle}>
            
                <TouchableOpacity onPress={this.props.showDropdown} 
                style={{
                    position: 'absolute',
                    top:0,
                    left:0,
                    right:0,
                    bottom:0,
                    zIndex: this.props.display? 0 : 1
                    }}>
                    <Text></Text>
                </TouchableOpacity>

                <FloatingInput label="Purpose*" floatColor = "#187FC0" red={this.props.red} value={this.props.value} editable={false}/>

                <View style={styles.arrow}></View>
                <View style={styles.listStyle}>
                    <TouchableOpacity onPress={() => {
                        this.props.purposeChange('Meeting')
                        this.props.hideDropdown()
                        }} style={styles.highlightStyle}>
                        <Text style={{...styles.textStyle, fontWeight: this.props.value== 'Meeting'? '700':'normal'}}>Meeting</Text>
                    </TouchableOpacity >
                    <TouchableOpacity onPress={() => {
                        this.props.purposeChange('Event')
                        this.props.hideDropdown()
                        }} style={styles.highlightStyle}>
                        <Text style={{...styles.textStyle, fontWeight: this.props.value== 'Event'? '700':'normal'}}>Event</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        this.props.purposeChange('Interview')
                        this.props.hideDropdown()
                        }} style={styles.highlightStyle}>
                        <Text style={{...styles.textStyle, fontWeight: this.props.value== 'Interview'? '700':'normal'}}>Interview</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {
                        this.props.purposeChange('Other')
                        this.props.hideDropdown()
                        }} style={styles.highlightStyle}>
                        <Text style={{...styles.textStyle, fontWeight: this.props.value== 'Other'? '700':'normal'}}>Other</Text>
                    </TouchableOpacity>
                </View>
            
            </View>
        )
    }
    
}

