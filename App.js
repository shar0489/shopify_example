import * as React from 'react';
import { StatusBar } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import HomeScreen from './screens/HomeScreen'
import QrScanner from './screens/QrScanner'
import GuesInfo from './screens/GuesInfo'
import Preview from './screens/Preview'
// import Print from './screens/Print'
import Finish from './screens/Finish'


const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator 
      initialRouteName="Home"
      screenOptions={{
        headerShown: false
      }}
      >

        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="QrScanner" component={QrScanner} />
        <Stack.Screen name="Guest" component={GuesInfo} />
        <Stack.Screen name="Preview" component={Preview} />
        {/* <Stack.Screen name="Print" component={Print} /> */}
        <Stack.Screen name="Finish" component={Finish} />
        
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
